angular.module('indexApp')
.controller('ProfileController', function($scope, $http, $cookies, $window, $location) {
	$scope.user = $cookies.get('logged_in');

	if($cookies.get("signed_in") == undefined || $cookies.get("signed_in") == "false") {
		window.location.href = "/login.html";
	}

	if (location.search.indexOf("page") <1 ) {
		window.location.href = "/profile.html?page=1";
	}

	param = location.search;
	$scope.page = param.substring(6,param.length);
	$scope.page = parseInt($scope.page);

	$http.get('/users/self/posts')
	.then(function(response){
		raw_array = response.data.posts.reverse();
		numPost = raw_array.length;
		pageTotal = (numPost % 5 == 0)? numPost/5 : (Math.floor(numPost/5) + 1);
		$scope.page_array = new Array(pageTotal);
		for (i=1; i<=pageTotal; i++){
			$scope.page_array[i-1] = i;
		}
		if ($scope.page == pageTotal && numPost % 5 != 0) {
			$scope.isFinal = true;
			$scope.posts_array = new Array(numPost % 5);
		} else {
			$scope.isFinal = ($scope.page == pageTotal) ? true : false;
			$scope.posts_array = new Array(5);
		}
		for (i = 0; i < $scope.posts_array.length; i++) {
			postIndex = ($scope.page - 1) * 5 + i;
			$scope.posts_array[i] = raw_array[postIndex];
			$scope.setTime($scope.posts_array[i].pdate, function(data){
				$scope.posts_array[i].timeDiff = data.timeDiff;
			})
		}
	})

	// get self infomation
	$http.get('/users/self')
	.then(function(response){
		$scope.email = response.data.user.email.substring(0,10);
		$scope.fbid = response.data.user.profile.facebook;
	})

	// get love signal infomation
	if($cookies.get("fbid") != undefined && $cookies.get("fbid") != ""){
		$http.get('/express-love/' + $cookies.get("fbid"))
		.then(function(response){
		if(response.data.feedback == "Success") {
			$scope.loveSignal = true;
        	$scope.lid = response.data.like._id;
			if(response.data.like.like_success == true) {
				$scope.love = true;
			}
			$http.get('/users/' + response.data.like.like_id)
			.then(function(response){
				$scope.loveUser = response.data.user.username;
			})
		} else {
			$scope.loveSignal = false;
		}
		})
	} else {
		$scope.loveSignal = false;
	}
    
	// function to determine the time difference
	$scope.setTime = function(pdate, callback){
  		now =+ new Date();
  		time = (now - pdate);
    	days = Math.floor(time/86400000);
   	 	hours = Math.floor((time/3600000) % 24);
    	minutes = Math.floor((time/60000) % 60);
    	if (days > 0) {
    		timeDiff = days + " day(s) ago";
    	} else if (hours > 0) {
    		timeDiff = hours + " hour(s) ago";
    	} else if (minutes > 0){
    		timeDiff = minutes + "minute(s) ago";
    	} else {
    		timeDiff = "just now";
    	}
    	callback({timeDiff: timeDiff});
  	}

	//update profile
	$scope.updateProfile = function() {
		$http.put('/users/update', {fbid: $scope.fbid})
		.then(function(response){
			//console.log(response);
			if (response.data.feedback != "Success") {
				console.log("Failed to update profile!");
			} else {
				$cookies.put("fbid", $scope.fbid);
				$window.location.reload();
			}
		})
	}

	// remove love signal
	$scope.removeLove = function() {
		$http.get('/express-love/delete/' + $scope.lid)
		.then(function(response){
			console.log(response);
		if (response.data.feedback != 'Success') {
			console.log('remove love signal failed!');
		} else {
			window.location.href = '/profile.html';
		}
    })
	}

	// sign out
	$scope.sign_out = function() {
		$http.get("/users/logout")
		.then(function(response) {
			console.log(response);
		if (response.data.feedback == "Success") {
			$scope.signed_in = false;
			$cookies.put("signed_in", $scope.signed_in);
			$cookies.remove("logged_in");
			$window.location.href = "/index.html";
		} else {
			console.log("logout failure")
		}
		});
	};
})
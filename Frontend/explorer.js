angular.module('explorerApp')
.controller('explorerController', function($scope, $cookies, $http) {
  	
  	if ($cookies.get("signed_in") == undefined || $cookies.get("signed_in") == "false") {
		window.location.href = "/login.html";
	};

  	if (location.search.indexOf("page") <1 ) {
  		window.location.href = "/explorer.html?page=1";
  	}
  	param = location.search;
  	$scope.page = param.substring(6,param.length);
  	$scope.page = parseInt($scope.page);
  	
  	$http.get("/allposts")
  	.then(function(response){
  		raw_array = response.data.reverse();
  		numPost = raw_array.length;
  		pageTotal = (numPost % 8 == 0)? numPost/8 : (Math.floor(numPost/8) + 1); // determine the total page number
  		$scope.page_array = new Array(pageTotal);
  		for (i=1; i<=pageTotal; i++){
  			$scope.page_array[i-1] = i;
  		}
  		if ($scope.page == pageTotal && numPost % 8 != 0) {
  			$scope.isFinal = true;
  			$scope.posts_array = new Array(numPost % 8);
  		} else {
  			$scope.isFinal = ($scope.page == pageTotal) ? true : false;
  			$scope.posts_array = new Array(8);
  		}
  		// define the posts to be shown in different pages
  		for (i=0; i < $scope.posts_array.length; i++) {
  			postIndex = ($scope.page - 1) * 8 + i;
  			$scope.posts_array[i] = raw_array[postIndex];
  			$scope.setTime($scope.posts_array[i].pdate, function(data){
  				$scope.posts_array[i].timeDiff = data.timeDiff;
  			})  			
  		}
  	})

  	// function that set the time difference expression
  	$scope.setTime = function(pdate, callback){
  		now =+ new Date();
  		time = (now - pdate);
    	days = Math.floor(time/86400000);
   	 	hours = Math.floor((time/3600000) % 24);
    	minutes = Math.floor((time/60000) % 60);
    	if (days > 0) {
    		timeDiff = days + " day(s) ago";
    	} else if (hours > 0) {
    		timeDiff = hours + " hour(s) ago";
    	} else if (minutes > 0){
    		timeDiff = minutes + "minute(s) ago";
    	} else {
    		timeDiff = "just now";
    	}
    	callback({timeDiff: timeDiff})
  	}

});
angular.module('indexApp')
.controller('LoveController', function($scope, $http, $timeout, $cookies, $window, $location) {
	// data initialize
	$scope.lovefbid = "";

	if($cookies.get("signed_in") == "false" || $cookies.get("signed_in") == undefined) {
		window.location.href = '/login.html';
	} else if ($cookies.get("fbid") == undefined || $cookies.get("fbid") == "") {
		window.alert("You cannot send love signal since you haven't linked your Facebook ID. \nPlease go to profile page and update the Facebook ID.");
	}

	// check the loverfbid input 
	$scope.validateLoverfbid = function(dirty){
		if($scope.loverfbid == undefined)
		{
	  		$scope.loverfbidValid = false;
	  		$scope.loverfbidErrorMsg = 'Please input the loverfbid';
		} else {
	  		$scope.loverfbidValid = true;
		}
  	}

  	// get love signal infomation
  	if($cookies.get("fbid") != undefined && $cookies.get("fbid") != ""){
    	$http.get('/express-love/' + $cookies.get("fbid"))
    	.then(function(response){
      		if(response.data.feedback == "Success") {
	        	$scope.loveSignal = true;
	        	//console.log(response);
	        	$scope.lid = response.data.like._id;
	        	if(response.data.like.like_success == true) {
	          		$scope.love = true;
	        	}
	        	$http.get('/users/' + response.data.like.like_id)
	        	.then(function(response){
	          	$scope.loveUser = response.data.user.username;
	        	})
      		} else {
        		$scope.loveSignal = false;
      		}
    	})
  	} else {
    	$scope.loveSignal = false;
  	}  

	// submit the data
	$scope.expressLove=function(){
		if ($cookies.get("signed_in")){
			$scope.uid = $cookies.get("uid");
			if ($cookies.get("fbid") != undefined && $cookies.get("fbid") != ""){
				$scope.fbid = $cookies.get("fbid");
				$http.post("/express-love/create", {
					'sent_id': $scope.uid,
					'sent_fbid': $scope.fbid,
					'like_fbid': $scope.loverfbid
				})
				.then(function(response){
					console.log("create response");
					console.log(response);
					if(response.data.feedback != 'Success') {
						console.log("Fail to create love signal");
						if (response.data.err == "like yourself") {
							window.alert("Please don't like yourself");
						} else if (response.data.err == "cannot find user") {
							window.alert("The Facebook ID doesn't linked to any WeLink account");
						}
					} else {
						$http.post("/express-love/check", response.data.like)
						.then(function(response){
							console.log("check response");
							console.log(response);
							if (response.data.feedback == 'Success' && response.data.status == 'Love') {
								$cookies.put("love", true);
							}
						})
					window.location.href = "/love-redirection.html";
					}
		 		})
			} else {
				window.location.href = "/profile.html";
			}
		} else {
			window.location.href = '/login.html';
		}	
	}

	// change the lover
	$scope.changeLove = function() {
		if ($scope.loverfbid == $cookies.get("fbid")) {
				window.alert("Please don't like yourself");
		} else {
			$http.post('/express-love/update/' + $scope.lid, {'like_fbid': $scope.loverfbid})
			.then(function(response){
				if (response.data.feedback == 'Success') {
					console.log(response);
					$http.post("/express-love/check", response.data.like)
					.then(function(response){
						console.log("check response");
						console.log(response);
						if (response.data.feedback == 'Success' && response.data.status == 'Love') {
							$cookies.put("love", true);
						}
					})
					window.location.href = "/love-redirection.html";
				} else if (response.data.err == "cannot find user") {
					window.alert("The Facebook ID doesn't linked to any WeLink account");
				}
			})
		}
	}

})
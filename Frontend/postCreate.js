var postCreate = function($scope, $http, $cookies) {
  	// user create post function
  	// get pic file
	$scope.fd = new FormData();
	if($cookies.get("signed_in") == undefined || $cookies.get("signed_in") == "false"){
		window.location.href = '/login.html';
	}
	$scope.uploadFile = function(files) {
		//Take the first selected file
		$scope.fd.append("pic", files[0]);
		console.log(files[0]);
		console.log($scope.fd.get("pic"));
  	};

  	// data initialize
  	$scope.title = "";
  	$scope.content = "";
  	//check the title
  	$scope.validateTitle = function(dirty){
		if($scope.title == undefined)
		{
	  		$scope.titleValid = false;
	  		$scope.titleErrorMsg = 'Title is required in JS';
		} else {
	  		$scope.titleValid = true;
		}
  	}
  	// check the content
  	$scope.validateContent = function(dirty){
		if($scope.content == undefined)
		{
	  		$scope.contentValid = false;
	  		$scope.contentErrorMsg = 'Content is required in JS';
		} else {
	  		$scope.contentValid = true;
		}
  	}

	// submit the data
	$scope.send_post = function() {
	  	console.log("send post");
	  	
	  	if ($cookies.get("signed_in")) 
	  	{
	  		$scope.uid = $cookies.get("uid");
		  	// send post to server
		  	$http.post("/posts/create", {
				'title': $scope.title,
				'content': $scope.content,
				'uid': $scope.uid
		  	})
		  	.then(function(response) {
				console.log(response);
				console.log("post");
				$scope.response = response;
				if ($scope.response.data.feedback == "Success") {
			  		var pid = $scope.response.data.post._id;
			  		// successfully post
			  		// if there is a picture 
				  	if ($scope.fd.get('pic')){
						// send pic to server
						$http.post('/posts/' + $scope.response.data.post._id + '/upload', $scope.fd,
						{
					  	withCredentials: true,
					  	headers: {'Content-Type': undefined },
					  	transformRequest: angular.identity
						})
						.then(function uploadSuccess(fileResponse) {
					  	console.log(fileResponse);
					  	if (fileResponse.data.feedback == "Success") {
							window.location.href = 'browse-edit.html?' + pid + "&page=1";
					  	}
						})
				  	} else {
						window.location.href = 'browse-edit.html?' + pid + "&page=1";
				  	}
				}
		 	})
		} else {
			window.location.href = '/login.html';
		}		  	
	}
}
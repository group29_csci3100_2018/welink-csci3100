angular.module('postApp')
.controller('loadController', function($scope, $cookies, $http) {

	$scope.user = $cookies.get("logged_in");

	if ($cookies.get("signed_in") == undefined || $cookies.get("signed_in") == "false") {
		window.location.href = "/login.html";
	};
	if (location.search.indexOf("page") <1 ) {
		window.location.href = window.location.href + "&page=1";
    }
	// get post_id
	$scope.isOwner = false;
	$scope.post = {};
	param = location.search.substring(1);  //need modification?
	$scope.post_id = param.substring(0, param.indexOf("&"));
	$scope.page = param.substring(param.indexOf("&")+6, param.length);
	$scope.page = parseInt($scope.page);

	// get detail information
	$http.get('/posts/' + $scope.post_id)
	.then(function(response) {
		$scope.post = response.data.post;
		console.log(response);
		if (response.data.err == "Invalid Post ID") {
			window.location.href = '/deleteSuccess.html';
		}
		if ($scope.post.pictures.length < 1) {
			$scope.has_pic = false;
		} else {
			$scope.has_pic = true;
		}
    // set the time difference
	now =+ new Date();
    time = (now - $scope.post.pdate);
    days = Math.floor(time/86400000);
    hours = Math.floor((time/3600000) % 24);
    minutes = Math.floor((time/60000) % 60);
    if (days > 0) {
    	$scope.timeDiff = days + " day(s) ago";
    } else if (hours > 0) {
    	$scope.timeDiff = hours + " hour(s) ago";
    } else if (minutes > 0){
    	$scope.timeDiff = minutes + "minute(s) ago";
    } else {
    	$scope.timeDiff = "just now";
    }
	}).then(function() {
		// get owner information
		$http.get('/users/' + $scope.post.uid)
		.then(function(response) {
			if (response.data.feedback == "Success") {
				$scope.owner = response.data;
				$scope.isOwner = ($scope.owner.user.username == $scope.user); //determine if user is Owner of post
			}
		});
	});

	// get comments
	$http.get('/posts/' + $scope.post_id + "/comments")
	.then(function(response) {
		raw_array = response.data.comments["0"].comments;    
		$scope.num_comments = raw_array.length;
		pageTotal = ($scope.num_comments % 10 == 0)? $scope.num_comments/10 : (Math.floor($scope.num_comments/10) + 1);
		$scope.page_array = new Array(pageTotal);
		for (i=1; i<=pageTotal; i++){
			$scope.page_array[i-1] = i;
		}
		if ($scope.page == pageTotal && $scope.num_comments % 10 != 0) {
			$scope.isFinal = true;
			$scope.comments_array = new Array($scope.num_comments % 10);
		} else {
			$scope.isFinal = ($scope.page == pageTotal) ? true : false;
			$scope.comments_array = new Array(10);
		}

		for (i=0; i < $scope.comments_array.length; i++) {
			commentIndex = ($scope.page - 1) * 10 + i;
			$scope.comments_array[i] = raw_array[commentIndex];	  			
		}
	})

	// delete post
	$scope.deletePost = function(){
		$http.get("/postsdelete/" + $scope.post_id)
		.then(function(response){
			if (response.data.feedback == "Failure"){
				console.log("Delete Fail");
			} else {
				window.location = "explorer.html";
			}
		})
	}

	// update post
	$scope.updatePost = function(){
    $http.put('/posts/' + $scope.post_id, {
		title: $scope.post.title,
		content: $scope.content
    })
    .then(function(response){
		if (response.data.feedback != 'Success') {
			console.log('Update Post Error');
		}
		else {
        window.location.href = '/browse-edit.html?' + $scope.post_id + '&page=1';
		}
    })
	}

	// add comment
	$scope.addComment = function() {
		console.log("add comment: " + $scope.comments);
		if ($scope.user == undefined) {
			$scope.comment_error = "you are not logged in now!";
		}
		else {
			$http.post("/posts/" + $scope.post_id + "/comments", {
			content: $scope.comments
		})
		.then(function(response) {
			console.log(response);
        window.location = "browse-edit.html?" + $scope.post_id + "&page=1";
		})
		}
	}

	// go to profile page
	$scope.goProfile = function() {
		if ($scope.isOwner) {
			window.location = "profile.html";
		}
		else {
			window.location = "profile.html?" + $scope.owner.user._id;
		}
	}
})

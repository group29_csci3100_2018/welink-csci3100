angular.module('indexApp')
.controller('LoginController', function($scope, $http, $timeout, $cookies, $window, $location) {
	$scope.login = function() {
		// show loading UI		
		// send login request to server
		$http.post("/users/login", {
			username: $scope.credential.username,
			password: $scope.credential.password,
		})
		.then(function(response) {
			$scope.login_response = response;
			// parse data from server response
			$scope.login_feedback = $scope.login_response.data.feedback;
			$scope.login_err_msg = $scope.login_response.data.err_msg;
			
			if ($scope.login_feedback == 'Success') {
				localStorage.setItem("is_manual_login", "true");
				$scope.loginFailed = false;
				$scope.signed_in = true;
				$cookies.put("signed_in", $scope.signed_in)

				// update frontend user status and corresponding UI
				var userName = $scope.credential.username;				
				$scope.sign_in(userName, function(data){
					if(!data.err){
						$window.location.href = '/index.html';
					}
				});				
			} else if ($scope.login_feedback == 'Failure') {
				$scope.loginFailed = true;
			} else {
				console.log('Error: invalid login feedback');
			}
		});
	};
	$scope.sign_in = function(client_name, callback) {
    	$http.get("/users/self")
    	.then(function(response) {
    		console.log(response);
	      	if (response.data.feedback == "Success") {
	        	$scope.uid = response.data.user._id;
	        	$scope.client_name = client_name;
	        	$cookies.put("fbid", response.data.user.profile.facebook);	       		
	       		$cookies.put("uid", $scope.uid);
	        	$cookies.put("logged_in", client_name);
	        if (localStorage.getItem("is_manual_login")) {
	          	localStorage.removeItem("is_manual_login");
	          	$window.location.reload();
	        }
	        callback({err: false});
	      } else {
	      	callback({err: true});
	      }
    	})
  	};
});


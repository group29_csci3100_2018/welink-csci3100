var mainController = function($scope, $http, $interval, $timeout, $cookies, $window) {
  
	// dynamically loaded file path and initiate parameter
	$scope.header = {url: "header.html"};
	$scope.foot = {url: "foot.html"};
	if ($cookies.get("signed_in") == undefined || $cookies.get("signed_in") == "false"){ 
		$scope.signed_in = false;
	} else {
		$scope.signed_in = $cookies.get("signed_in");
		$scope.uid = $cookies.get("uid");
	};
	$scope.username = $cookies.get("logged_in");

	var check_msg_promise;
	// function moved to login.js

	$scope.sign_in = function(client_name) {
		$http.get("/users/self")
		.then(function(response) {
			if (response.data.feedback == "Success") {
				$scope.uid = response.data.user._id;
				$scope.client_name = client_name;
				$scope.signed_in = true;
				$cookies.put("logged_in", client_name);
                check_msg_promise = $interval($scope.update_messenger, 1000);
				if (localStorage.getItem("is_manual_login")) {
					localStorage.removeItem("is_manual_login");
				}
			}
		})
	};
  
	$scope.sign_out = function() {
		$http.get("/users/logout")
		.then(function(response) {
			console.log(response);
		if (response.data.feedback == "Success") {
			$scope.signed_in = false;
			$cookies.put("signed_in", $scope.signed_in);
			$cookies.remove("logged_in");
			$cookies.remove("fbid");
			$window.location.href = "/index.html";
		} else {
			console.log("logout failure")
		}
		});
	};
};

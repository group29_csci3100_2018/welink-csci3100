/***********
 * Module for starting application
 *
 * module name: index
 * version: 0.0
 *
 * This module is the entry of the application. It's of the highest level and users other modules.
 * This module reads config file and set up all configurations and then start the application.
***********/

var express = require('express');
var app = express();
module.exports = app;

// load configurations
var config = require('../config');
for(var key in config) {
    app.set(key, config[key]);
}
require('./tools/signal');
var session = require('express-session');
app.use(session({
	secret: app.get('secret'),
	resave: false,
	saveUninitialized: false
}));
var body_parser = require('body-parser');
var cookieParser = require('cookie-parser');
app.use(cookieParser());
app.use(body_parser.json());
app.use(body_parser.urlencoded({extended: true}));
app.disable('x-powered-by');

//load modules
require('./db');
require('./view');
require('./resource');
require('./control');

app.listen(app.get('port'), function(){
    console.log('WeLink is running on port', app.get('port'));
});

/***********
 * Module for extending basic models
 *
 * module name: model_ext
 * version: 0.0
 *
 * This module provides more advanced funcitons for each model so that it will be much more convinient to manipulate data.
***********/
model = require('./model');
db = require('./db');
escape_html = require('escape-html');
app = require('./index');
mail_tool = require('./tools/send_email');

User = model.User;
Post = model.Post;
Like = model.Like;
Message = model.Message;
Comment = model.Comment;

// User model extension

//create a post
User.prototype.create_post = function(info, cb){
	Post.new_(info, function(result){
		cb(result);
	})
}

//create a like relationship
User.prototype.like = function(info, cb){
	var user = this;
	if(user._id.toString() == info.like_id)return cb({feedback: 'Failure', err_msg: 'Cannot express love to yourself'});
	Like.findOne({sent_id: info.sent_id, like_id: info.like_id}, function(err, like){
		if(err)return cb({feedback: 'Failure', err_msg: 'Fail to find like instance'});
		//check whether already liked
		if(like)return cb({feedback: 'Failure', err_msg: 'Like instance exists'});
		Like.new_(info, function(result){
			return cb(result);
		});
	})
}

//update a like relationship
Like.prototype.change = function(fbid, cb){
	var like = this;
	db.model('User').fbid_uid(fbid, function(result){
		if (result.feedback != 'Success') return cb(result);
			var uid = result.uid;
			like.like_id = uid;			
			if (like.like_success == true) {
				like.like_success = false;
				db.model('Like').byfbid(like.like_fbid, function(result){
					if (result.feedback != 'Success') return cb(result);
					like.like_fbid = fbid;
					like.save();
					var like_2 = result.like;
					like_2.like_success = false;
					like_2.save();
					return cb({feedback: 'Success', like: like});
				})
		} else {
			like.like_fbid = fbid;
			like.save();
			return cb({feedback: 'Success', like: like});
		}
	})
}

//add comment for a post
User.prototype.comment = function(info, cb){
	if(typeof(info.content) == 'undefined' || !info.content)return cb({feedback: 'Failure', err_msg: 'Invalid information'});
	var user = this;
	Post.get(info.pid, function(result){
		if(result.feedback != 'Success')return cb(result);
		var post=result.post;
		Comment.findById(post.comment_id, function(err, comment){
			if(err)return cb({feedback: 'Failure', err_msg: 'Fail to add comment'});
			comment.comments.set(comment.comments.length, [user._id, info.content, +new Date()]);
			comment.save(function(err, comment){
				if(err)return cb({feedback: 'Failure', err_msg: 'Fail to add comment'});
				User.get(post.uid, function(result){
					if(result.feedback != 'Success')return cb(result);
					var address = app.get('host')? (app.get('host') + ':' + app.get('port')) : 'localhost:'+app.get('port');
					var owner=result.user;
					var link='http://'+address+'/browse-edit.html?'+post._id;
					mail_tool.send_email(owner.email,'Someone comments on your post.',link,
						'<a href="'+link+'">Click to see detail!</a>', function(result){
						})
					return cb({feedback: 'Success', comment: comment});
				});
			});
		});
	})
}

module.exports.User = User;
module.exports.Post = Post;
module.exports.Message = Message;
module.exports.Like = Like;
module.exports.Comment = Comment;

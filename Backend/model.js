/********
 * Module to create models for database
 *
 * module name: model
 * version: 0.0
 *
 * This module defines models for database and provide some simple functions in OO way for use ease.
 * Models can filter bad input or invalid data from malicious or careless users.
********/

db = require('./db');//
escape_html = require('escape-html');

// User model
var User_Schema = db.Schema({
	username: {type: String, required: true, unique: true, match: /^[A-Za-z0-9_.]{3,20}$/},
	password: {type: String, required: true},
	history: [{type: db.Schema.Types.ObjectId, ref: 'Post', validate: {isAsync: true, validator: post_val}}],
	profile: {
		nickname: {type: String, match: /^[A-Za-z0-9_]{3,20}$/, default: ''},
		description: {type: String, match: /^.{0,300}$/, default: ''},
		phone: {type: String, match: /^\+?[\d\s]{3,20}$/, default: ''},
		wechat: {type: String, match: /^[A-Za-z0-9]{3:20}$/, default: ''},
		email: {type: String, match: /^[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}$/, default: ''},
		qq: {type: String, match: /^\d{4,12}$/, default: ''},
		facebook: {type: String, match: /^[A-Za-z0-9_.\s]{3,50}$/, default: ''}
	},
	email: {type: String, required: true, unique: true, match: /^1155\d{6}@link\.cuhk\.edu\.hk$/},
	like_signal: [{type: db.Schema.Types.ObjectId, ref: 'Like', validate: {isAsync: true, validator: like_val}}]
});

// User model: search by uid
User_Schema.statics.get = function(uid, cb){
	this.model('User').findById(uid, function(err, user){
		err_msg = 'Fail to find user.';
		if(err){
			if(err.message.indexOf('Cast') > -1){
				err_msg = 'Invalid User ID';
			}
			cb({feedback: 'Failure', err_msg: err_msg});
			return;
		}
		if(!user){
			err_msg = 'Invalid User ID';
			cb({feedback: 'Failure', err_msg: err_msg});
			return;
		}
		cb({feedback: 'Success', user: user});
		return;
	});
}
// User model: update profile
User_Schema.statics.update_profile = function(info, cb){
	this.model('User').findById(info.uid, function(err, user){
		if(err) return cb({feedback: "Failure", err:err});
		user.profile.facebook = info.fbid;
		user.save(function(err, user){
			if(err) return cb({feedback: "Failure", err:err});
			return cb({feedback: "Success", user:user});
		})
	})
}
// User model: create new instance
User_Schema.statics.new_ = function(info, cb){
	var User = this.model('User');
	info.history = []
	info.like = []
	User.create(info, function(err, user){
		err_msg = 'Fail to create user';
		if(err){
			if(err.message.indexOf('duplicate') > -1){
				err_msg = 'The username is taken';
				if(err.message.indexOf('$email') > -1)err_msg = 'The email is taken';
			}
			if(err.message.indexOf('validation') > -1){
				err_msg = 'Invalid information';
			}
			cb({feedback: 'Failure', err_msg: err_msg});
			return;
		}
		cb({feedback: 'Success', user: user});
		return;
	});
}

//user model: get uid from fbid
User_Schema.statics.fbid_uid = function(fbid, cb){
	this.model('User').findOne({'profile.facebook': fbid}, function(err, user){
		if (err) return cb({feedback: 'Failure', err: err});
		if (!user) return cb({feedback: 'Failure', err: "cannot find user"});
		return cb({feedback: 'Success', uid: user._id});
	})
}

// validation function for attribute, in case of JSON parse injection
function str_array(v){
	if(v.indexOf('title') < 0)return false;
	if(v.indexOf('description') < 0)return false;
	if(v.indexOf('condition') < 0)return false;
	for(var i=0;i<v.length;i++){
		if(!/^[A-Za-z0-9\s]{3,20}$/.test(v[i]))return false;
	}
	return true;
}
function pos_val(v){
	return (v >= 0)
}

// Post Model
var Post_Schema = db.Schema
({
	uid: {type: db.Schema.ObjectId, ref: 'User', required: true, validate: {isAsync: true, validator: user_val}},
	pdate: {type: Number, default: 0},
	title: {type: String, required: true},
	content: {type: String, required: true},
	comment_id : [{type: db.Schema.ObjectId, ref: 'Comment', validate: {isAsync: true, validator: comment_val}}],
	pictures: [{type: Number}],
});

// Post model: create new instance
Post_Schema.statics.new_ = function(info, cb){
	var Post = this.model('Post')
	timestamp = +new Date();
	info.pdate = timestamp;
	delete info.comments;
	delete info.comment_id;
	Post.create(info, function(err, post){
		err_msg = 'Fail to create post';
		if(err){
			if(err.message.indexOf('validation') > -1){
				err_msg = 'Invalid information';
			}
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		var Comment = db.model('Comment', Comment_Schema);
		Comment.new_({pid: post._id}, function(result){
			if(result.feedback != 'Success'){
				post.remove();
				return cb({feedback: 'Failure', err_msg: err_msg});
			}
			post.comment_id = result.comment._id;
			post.save(function(err, post){
				if(err)return cb({feedback: 'Failure', err_msg: err_msg});
			});
			return cb({feedback: 'Success', post: post});
		});
	});
}

// Post model: get by post id
Post_Schema.statics.get = function(pid, cb){
	this.model('Post').findById(pid, function(err, post){
		err_msg = 'Fail to find post.';
		if(err){
			if(err.message.indexOf('Cast') > -1){
				err_msg = 'Invalid Post ID';
			}
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		if(!post){
			err_msg = 'Invalid Post ID';
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		return cb({feedback: 'Success', post: post});
	});
}

// Post model: edit post content
Post_Schema.statics.edit_content = function(info, cb){
	
	this.model('Post').findById(info.pid, function(err, post){
		err_msg = 'Fail to find post.';
		if(err){
			if(err.message.indexOf('Cast') > -1){
				err_msg = 'Invalid Post ID';
			}
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		if (typeof(info.content) !== 'undefined' && info.content){
			post.content = info.content;
		}
		if(typeof(info.title) !== 'undefined' && info.title){
			post.title = info.title;
		}
		post.save(function(err, post){
		if(err){
			err_msg = 'Fail to update content';
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		return cb({feedback: 'Success', post: post});
		});
	})	
}

// Post model: delete post
Post_Schema.statics.delete_ = function(pid, cb){
	this.model('Post').deleteOne({"_id": pid}, function(err){
		if(err){
			err_msg = 'Fail to delete post';
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		return cb({feedback: 'Success'});
	});
}

// Post model: find all posts
Post_Schema.statics.findAll = function(cb){
	this.model('Post').find(function(err, post){
		if(err){
			return cb({feedback: 'Failure', err_msg: 'Fail to get post info'});
		}
		return cb({feedback: 'Success',post: post});
	});
}

// Post model: find all posts of me
Post_Schema.statics.findAllMe = function(uid, cb){
	this.model('Post').find({uid: uid}, function(err, posts){
		if(err){
			return cb({feedback: 'Failure', err_msg: 'Fail to get post info'});
		}
		return cb({feedback: 'Success',posts: posts});
	})
}

// Comment Model
var Comment_Schema = db.Schema({
	pid: {type: db.Schema.ObjectId, required: true, validate: {isAsync: true, validator: post_val}},
	comments: {type: Array, default: []},
});
// Comment model: create new instance
Comment_Schema.statics.new_ = function(info, cb){
	delete info.comments;
	var Comment = this.model('Comment');
	Comment.create(info, function(err, comment){
		err_msg = 'Fail to create comment';
		if(err){
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		return cb({feedback: 'Success', comment: comment});
	});
}

// Message model
var Message_Schema = db.Schema({
	uid1: {type: db.Schema.ObjectId, ref: 'User', required: true, validate: {isAsync: true, validator: user_val}},
	uid2: {type: db.Schema.ObjectId, ref: 'User', required: true, validate: {isAsync: true, validator: user_val}},
	messages: {type: Array, default: []},
});
// Message model: get message by id
Message_Schema.statics.get = function(mid, cb){
	this.model('Message').findById(mid, function(err, message){
		err_msg = 'Fail to find message';
		if(err){
			if(err.message.indexOf('Cast') > -1){
				err_msg = 'Invalid Message ID';
			}
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		if(!message){
			err_msg = 'Invalid Message ID';
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		return cb({feedback: 'Success', message: message});
	});
}
// Message model: create new instance
Message_Schema.statics.new_ = function(info, cb){
	delete info.messages;
	var Message = this.model('Message');
	Message.create(info, function(err, message){
		err_msg = 'Fail to create message';
		if(err){
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		return cb({feedback: 'Success', message: message});
	});
}

// Like model
var Like_Schema = db.Schema({
	sent_id: {type: db.Schema.ObjectId, ref: 'User', required: true, unique: true, validate: {isAsync: true, validator: user_val}},
	sent_fbid: {type: Number, required: true},
	like_id: {type: db.Schema.ObjectId, ref: 'User', required: true, validate: {isAsync: true, validator: user_val}},
	like_fbid: {type: Number, required: true},
	timestamp: {type: Number, required: true},
	like_success: {type: Boolean}
});
// Like model: search by id
Like_Schema.statics.get = function(lid, cb){
	this.model('Like').findById(lid, function(err, like){
		err_msg = 'Fail to find like instance';
		if(err){
			if(err.message.indexOf('Cast') > -1){
				err_msg = 'Invalid Like ID';
			}
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		if(!like){
			err_msg = 'Invalid Like ID';
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		return cb({feedback: 'Success', like: like});
	});
}

// Like model: search by sent_fbid
Like_Schema.statics.byfbid = function(fbid, cb){
	this.model('Like').findOne({'sent_fbid':fbid}, function(err, like){
		if (err) return cb({feedback: 'Failure', err: err});
		if (!like) return cb({feedback: 'Failure', err: 'No like signal'});
		return cb({feedback: 'Success', like: like});
	})
}

// Like model: create new instance
Like_Schema.statics.new_ = function(info, cb){
	var Like = this.model('Like');
	info.timestamp = +new Date();
	info.like_success = false;
	Like.create(info, function(err, like){
		err_msg = 'Fail to create like instance';
		if(err){
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		return cb({feedback: 'Success', like: like});
	});
}

// Like model: cross check
Like_Schema.statics.check_ = function(info, cb){
	this.model('Like').findById(info._id, function(err, like){
		if (err) return cb({feedback: 'Failure', err: err});
		if (like.like_success == true) {
			return cb({feedback: 'Success', status: 'Love'});
		} else {
			db.model('Like').findOne({"sent_fbid":like.like_fbid}, function(err, like_2){
				if (err) return cb({feedback: 'Failure', err: 'Fail to find the like instance'});
				if (!like_2) return cb({feedback: 'Failure', err: 'Target has no like'});
				if (like_2.like_fbid == info.sent_fbid) {
					like_2.like_success = true;
					like_2.save();
					like.like_success = true;
					like.save();
					return cb({feedback: 'Success', status: 'Love'});
				} else {
					return cb({feedback: 'Success', status: 'Single'});
				}
			});
		}
	});
}

// Like model: delete instance
Like_Schema.statics.delete_ = function(lid, cb){
	this.model('Like').findById(lid, function(err, like){
		if (like.like_success == true) {
			db.model('Like').findOne({'sent_fbid': like.like_fbid}, function(err, like_2){
				like_2.like_success = false;
				like_2.save();
			})
		}
	});
	this.model('Like').deleteOne({"_id": lid}, function(err){
		if(err){
			err_msg = 'Fail to delete like signal';
			return cb({feedback: 'Failure', err_msg: err_msg});
		}
		return cb({feedback: 'Success'});
	});
}

function user_val(uid, cb){
	this.model('User').findById(uid, function(err, user){
		if(err)return cb(false);
		if(!user)return cb(false);
		return cb(true);
	})
}
function post_val(pid, cb){
	this.model('Post').findById(pid, function(err, post){
		if(err)return cb(false);
		if(!post)return cb(false);
		return cb(true);
	})
}
function comment_val(coid, cb){
	this.model('Comment').findById(coid, function(err, comment){
		if(err)return cb(false);
		if(!comment)return cb(false);
		return cb(true);
	})
}
function like_val(lid, cb){
	this.model('Like').findById(lid, function(err, like){
		if (err) return cb(false);
		if(!like)return cb(false);
		return cb(true);
	})
}


var User = db.model('User', User_Schema);
var Post = db.model('Post', Post_Schema);
var Comment = db.model('Comment', Comment_Schema);
var Message = db.model('Message', Message_Schema);
var Like = db.model('Like', Like_Schema);
module.exports.User = User;
module.exports.Post = Post;
module.exports.Comment = Comment;
module.exports.Message = Message;
module.exports.Like = Like;

/***********
 * Module for providing restful api
 *
 * module name: resource
 * version: 0.0
 *
 * This module provides implements of restful api for users
 * calling each function will return a JSON feedback which contains key information that users ask for
***********/

var app = require('./index');
var path = require('path');
var control = require('./control');
var model = require('./model_ext');
var check_login = control.check_login;
var async = require('async');


// Picture resource
app.get('/posts/:pid/pictures/:p', function(req, res){
	var pid = req.params.pid;
	var p = req.params.p;
	model.Post.get(pid, function(result){
		if(result.feedback != 'Success')return res.send({feedback: 'Failure'});
		var post = result.post;
		var i = post.pictures.indexOf(p);
		if(i < 0)return res.send({feedback: 'Failure'});
		res.sendFile(path.join(__dirname, 'uploads', post._id.toString(), p+'.jpg'));
	});
});
//get the thumbnail of an item picture
app.get('/posts/:pid/thumbnails/:p', function(req, res){
	var pid = req.params.pid;
	var p = req.params.p;
	model.Post.get(pid, function(result){
		if(result.feedback != 'Success')return res.send({feedback: 'Failure'});
		var post = result.post;
		var i = post.pictures.indexOf(p);
		if(i < 0)return res.send({feedback: 'Failure'});
		res.sendFile(path.join(__dirname, 'uploads', post._id.toString(), p+'_thumbnail.jpg'));
	});
});

// Comment resource
app.get('/posts/:pid/comments', function(req, res){
	var pid = req.params.pid;
	//first check whether the Post exists
	model.Post.get(pid, function(result){
		if(result.feedback != 'Success') {
			return res.send({feedback: 'Failure'});
		}
		var post = result.post;
		//replace the comment_id with the corresponding comment
		post.populate('comment_id', function(err, post){
			if(err)return res.send({feedback: 'Failure'});
			res.send({feedback: 'Success', comments: post.comment_id});
		});
	})
})
// create comments
app.post('/posts/:pid/comments', function(req, res){
	if(!check_login(req, res))return;
	var pid = req.params.pid;
	var content = req.body.content;
	var uid = req.cookies.uid;
	model.User.get(uid, function(result){
		if(result.feedback != 'Success')return res.send({feedback: 'Failure'});
		var user = result.user;
		//post a comment
		user.comment({pid: pid, content: content}, function(result){
			if(result.feedback != 'Success')return res.send({feedback: 'Failure'});
			return res.send({feedback: 'Success', comments: result.comment.comments});
		})
	})
});

//get post details
app.get('/posts/:pid', function(req, res){
	var pid = req.params.pid;
	//check whether the post exists
	model.Post.get(pid, function(result){
		if(result.feedback != 'Success') {
			return res.send({feedback: 'Failure', err: result.err_msg});
		}
		var post = result.post;
		//replace the comment_id with the according comment
		post.populate('comment_id').populate('cid', function(err, post){
			if(err) {
				return res.send({feedback: 'Failure'});
			}
			var post = post.toObject();
			try{
				delete post.__v;
				delete post.comment_id.__v;
				delete post.comment_id.pid;
			} catch(e){
				return res.send({feedback: 'Failure'});
			}
			return res.send({feedback: 'Success', post: post});
		})
	})
})
//update post info
app.put('/posts/:pid', function(req, res){
	if(!check_login(req, res)) return;
	var pid=req.params.pid;
	var info=req.body;
	info.pid = pid;
	//check whether the post exists
	model.Post.get(pid, function(result){
		if(result.feedback != 'Success') return res.send({feedback: 'Failure'});
		var post=result.post;
		//check owner of the post
		if(post.uid != req.cookies.uid)return res.send({feedback: 'Failure'});
		model.Post.edit_content(info, function(result){
			if(result.feedback != 'Success')return res.send({feedback: 'Failure'});
			res.send(result);
		})
	})
})
//delete post
app.get('/postsdelete/:pid', function(req, res){
	if(!check_login(req, res)) return;
	var pid=req.params.pid;
	//check whether the post exists
	model.Post.get(pid, function(result){
		if(result.feedback != 'Success') return res.send({feedback: 'Failure'});
		var post=result.post;
		//check owner of the post
		if(post.uid != req.cookies.uid) return res.send({feedback: 'Failure'});
		model.Post.delete_(pid, function(result){
			if(result.feedback != 'Success')return res.send({feedback: 'Failure'});
			res.send(result);
		})
	})
})

//create love signal
app.post('/express-love/create', function(req, res){
	var info = req.body;
	model.User.fbid_uid(info.like_fbid, function(result){
		if (result.feedback != 'Success') return res.send({feedback: 'Failure', err: result.err});
		if (result.uid == info.sent_id) return res.send({feedback: 'Failure', err: "like yourself"});
		info.like_id = result.uid;
		model.User.get(info.sent_id, function(result){
			if(result.feedback != 'Success')return res.send({feedback: 'Failure', err: 'Cannot find user'});
			var user = result.user;
			user.like(info, function(result){
				if(result.feedback != 'Success') return res.send({feedback: 'Failure', err: 'Cannot create like'});
				return res.send(result);
			})
		})
	})
})

// update the love signal
app.post('/express-love/update/:lid', function(req, res){
	var lid = req.params.lid;
	var info = req.body;
	model.Like.get(lid, function(result){
		if (result.feedback != "Success") return res.send({feedback: 'Failure', err: result.err_msg});
		var like = result.like;
		like.change(info.like_fbid, function(result){
			return res.send(result);
		})
	})
})

// check whether love each other
app.post('/express-love/check', function(req, res){
	var info = req.body;
	model.Like.check_(info, function(result){
		if (result.feedback != "Success") return res.send({feedback: 'Failure', err: result.err});
		return res.send(result);
	})
})

// get the love status
app.get('/express-love/:fbid', function(req, res){
	if(!check_login(req, res)) return;
	var fbid = req.params.fbid;
	model.Like.byfbid(fbid, function(result){
		res.send(result);
	})
})

// delete love signal
app.get('/express-love/delete/:lid', function(req, res){
	if(!check_login(req, res)) return;
	var lid = req.params.lid;
	model.Like.delete_(lid, function(result){
		if(result.feedback != 'Success')return res.send({feedback: 'Failure', err: result.err_msg});
		return res.send({feedback: 'Success'});
	})
})

// Post create
app.post('/posts/create', function(req, res){
	if(!check_login(req, res)){
		return res.send({feedback: 'Failure', error: 'Not log in'});
	};
	var info = req.body;
	model.User.get(info.uid, function(result){
		if(result.feedback != 'Success')return res.send({feedback: 'Failure'});
		var user = result.user;
		user.create_post(info, function(result){
			if(result.feedback != 'Success')return res.send({feedback: 'Failure'});
			return res.send(result);
		})
	})
})

// get all posts
app.get('/allposts', function(req,res){
	if (!check_login(req,res)) {
		return res.send({feedback: 'Failure', error: 'Not log in'});
	};
	model.Post.findAll(function(result){
		if (result.feedback != 'Success') return res.send({feedback: 'Failure'});
		return res.send(result.post);
	})
})


//get another user's info
app.get('/users/:uid', function(req, res, next){
	var uid = req.params.uid;
	if(!db.Types.ObjectId.isValid(uid) || uid == 'new_messages')return next();
	if(!check_login(req, res))return;
	model.User.findById(uid, '_id username email profile', function(err, user){
		if(err)return res.send({feedback: 'Failure', err_msg: 'Invalid information'});
		return res.send({feedback: 'Success',user:user});
	});
})


//get self info
app.get('/users/self', function(req, res){
	if(!check_login(req, res)){
		return
	};
	var uid = req.cookies.uid;
	model.User.findById(uid, '_id username email profile msg_buf history', function(err, user){
		if(err){
			res.clearCookie('uid');
			res.cookie('signed_in', false);
			return res.send({feedback: 'Failure', err_msg: 'Invalid information'});
		}
		return res.send({feedback: 'Success',user:user});
	})
})

//update user info
app.put('/users/update', function(req, res){
	if(!check_login(req, res))return;
	var uid = req.cookies.uid;
	var info = req.body;
	info.uid = uid;
	//check user existence
	model.User.get(uid, function(result){
		if(result.feedback != 'Success') return res.send(result);
		var user = result.user;
		//get profile attributes
		model.User.update_profile(info, function(result){
			if (result.feedback != "Success") {
				res.send({feedback: "Failure"});
			} else {
				res.send({feedback: "Success", user: result.user});
			}
		})
	})
})

//get posts of me
app.get('/users/self/posts',function(req, res){
	if(!check_login(req, res)) return;
	var uid=req.cookies.uid;
	//replace cid by the corresponding category
	model.Post.findAllMe(uid, function(result){
		err_msg= 'Fail to get posts of this user.'
		if(result.feedback == 'Failure'){
			//may change err_msg
			return res.send({feedback: 'Failure', err_msg: err_msg});
		}
		return res.send({feedback: 'Success', posts: result.posts});
	})
})
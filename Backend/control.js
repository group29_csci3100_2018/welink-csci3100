var app = require('./index');
var multer = require('multer');
var model = require('./model_ext');
var fs = require('fs');
var path = require('path');
var mkdirp = require('mkdirp');
var sharp = require('sharp');
var upload = multer({dest: '../tmp'});
var crypto = require('crypto');
var send_email = require('./tools/send_email');

function encrypt(text){
	var cipher = crypto.createCipher(app.get('encrypt_algo'), app.get('secret'));
	var crypted = cipher.update(text,'utf8','hex');
	crypted += cipher.final('hex');
	return crypted;
}
function decrypt(text){
	var decipher = crypto.createDecipher(app.get('encrypt_algo'), app.get('secret'));
	var dec = decipher.update(text,'hex','utf8')
	dec += decipher.final('utf8');
	return dec;
}

function check_login(req, res){
	if(!req.cookies.signed_in){
		return false;
	}
	return true
}
module.exports.check_login = check_login

//temp picture remove
function rm_tmp_pic(req){
	var files = req.files;
	for(var i=0; i<files.length; i++){
		fs.unlink(files[i].path, function(err){
			if(err)console.log(err);
		});
	}
}

//temp picture move
function mv_tmp_pic(post, post_path, i, files, req, res){// leave dirty file
	if(i>=files.length) return;

	var len = post.pictures.length;
	if(len == 0) post.pictures.set(len, 0);
	else post.pictures.set(len, post.pictures[len-1]+1);

	var size;
	var pic = sharp(files[i].path);
	pic.metadata(function(err, metadata){
		if(err || !(metadata.format == 'jpg' || metadata.format == 'jpeg')){
			rm_tmp_pic(req);
			return res.send({feedback: 'Failure', err_msg: 'Invalid extension'});
		}
		size = metadata.width > metadata.height ? metadata.height: metadata.width;
		pic.resize(size, size).toFile(path.join(post_path, post.pictures[len]+'.jpg'), function(err){
			if(err){
				rm_tmp_pic(req);
				return res.send({feedback: 'Failure', err_msg: 'Fail to save pictures'});
			}
			if(i == files.length-1)post.save(function(err){
				if(err){
					rm_tmp_pic(req);
					return res.send({feedback: 'Failure', err_msg: 'Fail to save pictures'});
				}
				rm_tmp_pic(req);
				return res.send({feedback: 'Success'});
			})
			else mv_tmp_pic(post, post_path, i+1, files, req, res);
		});
		pic.resize(200, 200).toFile(path.join(post_path, post.pictures[len]+'_thumbnail.jpg'));
	});
}

app.delete('/posts/:pid/pictures/:p', function(req, res){
	if(!check_login(req, res))return;
	var pid = req.params.pid;
	var p = req.params.p;
	model.Post.get(pid, function(result){
		if(result.feedback != 'Success')return res.send(result);
		var post = result.post;
		var i = post.pictures.indexOf(p);
		if(i < 0)return res.send({feedback: 'Failure', err_msg: 'Fail to delete picture'});
		post.pictures.splice(i, 1);
		post.save(function(err){
			if(err)return res.send({feedback: 'Failure', err_msg: 'Fail to delete picture'});
			var post_path = path.join(__dirname, '..', 'Frontend', 'uploads', post._id.toString());
			fs.unlink(path.join(post_path, p+'.jpg'), function(err){
				if(err)console.log(err);
			});
			fs.unlink(path.join(post_path, p+'_thumbnail.jpg'), function(err){});
			return res.send({feedback: 'Success'});
		});
	})
});

var upload_pic = upload.array('pic', app.get('post_pic_size'));
app.post('/posts/:pid/upload', function(req, res){// pictures to jpg
	upload_pic(req, res, function(err){
		if(err){
			rm_tmp_pic(req);
			return res.send({feedback: 'Failure', err_msg: 'Exceed upload max count'});
		}
		if(!check_login(req, res))return rm_tmp_pic(req);

		var valid_ext = ['jpg', 'jpeg', 'png'];
		var files = req.files;
		for(var i=0; i<files.length; i++){
			var strs = files[i].originalname.split('.');
			if(valid_ext.indexOf(strs[strs.length -1].toLowerCase()) < 0){
				rm_tmp_pic(req);
				return res.send({feedback: 'Failure', err_msg: 'Invalid extension'});
			}
		}

		var pid = req.params.pid;
		model.Post.get(pid, function(result){
			if(result.feedback != 'Success'){
				rm_tmp_pic(req);
				return res.send(result);
			}
			var uid = req.cookies.uid;
			var post = result.post;
			if(!post.uid.equals(uid)){
				rm_tmp_pic(req);
				return res.send({feedback: 'Failure', err_msg: 'Invalid information'});
			}
			if(post.pictures.length+files.length > app.get('post_pic_size')){
				rm_tmp_pic(req);
				return res.send({feedback: 'Failure', err_msg: 'Exceed upload max count'});
			}
			var post_path = path.join(__dirname, '..', 'Frontend','uploads', post._id.toString());
			console.log("dir name:"+__dirname);
			console.log("post_path is:");
			console.log(post_path);
			mkdirp(post_path, function(err){});
			mv_tmp_pic(post, post_path, 0, files, req, res);
		})
	});
});

app.post('/users/register', function(req, res){// vulnerable to DOS
	var username = req.body.username;
	var password = req.body.password;
	var email = req.body.email;
	try{
		if(!/^1155\d{6}@link\.cuhk\.edu\.hk/.test(email) || !/^[A-Za-z0-9_]{3,20}$/.test(username) || password.length < 8)return res.send({feedback: 'Failure', err_msg: 'Invalid information'});
		var hash = crypto.createHash('sha256').update(password).digest('base64');
		var encoded = encrypt(email)+'|'+encrypt(hash)+'|'+encrypt(username)+'|'+encrypt((+new Date()).toString());
		send_email.activate_email(email, encoded, function(result){
			if(result.feedback != 'Success')return res.send({feedback: 'Failure', err_msg: 'Fail to send email'});
			return res.send({feedback: 'Success'});
		});
	}catch(e){
		console.log(e);
		return res.send({feedback: 'Failure', err_msg: 'Invalid information'});
	}
});
app.post('/users/validate', function(req, res){// vulnerable to DOS and info listing
	var username = req.body.username;
	var email = req.body.email;
	var condition;
	if(username)condition = {username: username};
	else if(email)condition = {email: email};
	else return res.send({feedback: 'Failure'});
	model.User.findOne(condition, function(err, user){
		if(err)return res.send({feedback: 'Failure'});
		if(user)return res.send({feedback: 'Success', msg: 'taken'});
		return res.send({feedback: 'Success', msg: 'available'});
	})
})
app.get('/users/activate', function(req, res){// redirect
	var info = req.query.text;
	try{
		info = info.split('|');
		var username = decrypt(info[2]);
		var email = decrypt(info[0]);
		var hash = decrypt(info[1]);
		var timestamp = parseInt(decrypt(info[3]));
		var now = +new Date();
		if((now - timestamp) > 10*60*1000)return res.send({feedback: 'Failure', err_msg: 'Link expires'});
		model.User.findOne({username: username, email: email}, function(err, user){
			if(err)return res.send({feedback: 'Failure', err_msg: 'Fail to activate'});
			if(user)return res.send({feedback: 'Failure', err_msg: 'Link expires'});
			model.User.new_({username: username, email: email, password: hash}, function(result){
				if(result.feedback != 'Success')return res.send({feedback: 'Failure', err_msg: 'Fail to activate'});
				res.cookie('uid', result.user._id);
				return res.redirect('/activateSuccess.html');// activate successful page
			});
		})
	}catch(e){
		return res.send({feedback: 'Failure', err_msg: 'Fail to activate'});
	}
})
app.post('/users/login', function(req, res){
	var username = req.body.username;
	var password = req.body.password;
	try{
		var hash = crypto.createHash('sha256').update(password).digest('base64');
	}catch(e){
		console.log(e);
		return res.send({feedback: 'Failure', err_msg: 'Invalid information'});
	}
	var condition;
	if(username)condition = {username: username, password: hash};
	else if(email)condition = {email: email, password: hash};
	else return res.send({feedback: 'Failure', err_msg: 'Fail to login'});
	model.User.findOne(condition, function(err, user){
		if(err)return res.send({feedback: 'Failure', err_msg: 'Fail to login'});
		if(!user)return res.send({feedback: 'Failure', err_msg: 'Fail to login'});
		res.cookie("uid", user._id);
		return res.send({feedback: 'Success'});// welcome page
	});
})
app.get('/users/logout', function(req, res){
	req.session.destroy(function(err){
		if(err)return res.send({feedback: 'Failure'});
		res.clearCookie('uid');
		res.cookie('signed_in', false);
		return res.send({feedback: 'Success'});
	})
})
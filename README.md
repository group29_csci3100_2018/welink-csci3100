# WeLink
一个树洞型Web App，灵感来自CU转换器

- - - -
## Done

1. 完成了HTML基本构建

4. Register comes online, email validation online, model successfully integrated.

- - - -
## To-do


4. The "Cancel" button in the register.html need to configured [done] 
5. Model may or may not need to be modified.
6. Think about how to implement the lover matching (the current model is based on follow relationship, which should be similar)

- - - -
## Optional advanced frontend features
1. 在【帖子列表】显示评论数量

- - - -
## Useful links

1. [介绍Bootstrap基本操作的Tania博客，写的很清楚](https://www.taniarascia.com/what-is-bootstrap-and-how-do-i-use-it/)
2. [Bootstrap官方的组件基本介绍](https://getbootstrap.com/docs/3.3/components/)
3. [Bootstrap官方的表单等组件介绍，做register部分应该用到](https://getbootstrap.com/docs/4.0/components/forms/)
4. [Quora上关于如何reuse HTML组件的介绍(jQuery)，点进博客链接看](https://www.quora.com/What-is-the-best-way-to-create-reusable-HTML-templates-components-like-nav-bar-footer-customer-comments-that-can-be-reused-across-the-site)
5. [一些social media的图标](https://www.w3schools.com/icons/fontawesome_icons_brand.asp)
6. [w3schools.com jQuery教程](https://www.w3schools.com/jquery/jquery_get_started.asp)
7. [一个material design的bootstrap，也许可以用](https://fezvrasta.github.io/bootstrap-material-design/docs/4.0/getting-started/download/)

